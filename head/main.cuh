#pragma once




// utilile
#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include "StudyCase.h"
#include "Simparam.h"
#include "System.h"



//OPF
#include "OPFADMM.h"
#include "OPFADMM2.h"
#include "OPFADMMGPU.cuh"
#include "OPFADMMGPU2.cuh"




#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cuda_runtime.h>
#include <cudaProfiler.h>





// fichier de test
#include "TestUtilities.cuh"






void SimuCompareISGT();
void testOPF();
