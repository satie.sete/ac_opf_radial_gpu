#pragma once
#include "MethodOPF.h"
#include <iostream>
#include <string>
#include <chrono>
#include "Utilities.cuh"
#include "kernelFunction.cuh"


class OPFADMMGPU : public MethodOPF
{
public:
	OPFADMMGPU();
	OPFADMMGPU(float rho);
	virtual ~OPFADMMGPU();
	void setParam(float rho);
	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas);
	virtual void updateP0(const StudyCase& cas);
	virtual void init(const Simparam& sim, const StudyCase& cas);

	virtual void solveConsensus(float eps, MatrixCPU* PSO);
	virtual void initConsensus(const Simparam& sim, const StudyCase& cas, float rhoSO);
	virtual void updateConsensus(MatrixCPU* Pmarket);

	bool chekcase();
	std::string NAME ="OPFADMMGPU";
	void updateGlobalProb();
	void updateLocalProb(float epsL, int nIterL);
	void updateMu();

	float getPLoss();
	float getQLoss();
	void ComputePFromAgentToBus();

	void updateChat();
	void CommunicationX();
	float updateRes(int indice);
	virtual int feasiblePoint();

	void display();
private:
	// ne change pas avec P0
	int _blockSize = 512;
	int _blockSizeSmall = 64;
	int _numBlocksB;
	int _numBlocksN;
	int _numBlocksM;

	int _nBus = 0;
	int _nLine = 0;
	int _nAgent = 0;
	int _sizeOPFTotal = 0;
	int _sizeOPFMax = 0;
	float _rho = 0;
	float _rhoInv = 0;
	MatrixGPU root; // peut �tre juste defini en local c'est suffisant
	MatrixGPU coefPoly2;
	MatrixGPU coefPoly3;
	float _rhol = 0;
	int _iterLocal = 0;
	int _iterGlobal = 0;
	int _iterG = 0;
	int _stepG = 0;
	float _mu = 40;
	float _tau = 2;
	bool consensus = false;
	clock_t timeOPF = 0;
	
	MatrixGPU _apt1;

	// parameter agent and iteration dependant (but not here for now)


	MatrixGPU tempN1; // Matrix temporaire pour aider les calculs
	MatrixGPU tempNN; // plut�t que de re-allouer de la m�moire � chaque utilisation
	MatrixGPU tempM1; //
	MatrixGPU tempM; //

	

	MatrixGPU Cost1;
	MatrixGPU Cost2;
	MatrixGPU _CoresBusAgent;
	MatrixGPU _nAgentByBus;

	MatrixGPU ZsRe;
	MatrixGPU ZsIm;
	MatrixGPU ZsNorm;
	MatrixGPU VoltageLimit; // (vmin^2, vmax^2) * sqrt(Nchild + 1 / 2)
	MatrixGPU VoltageLimitReal; // vmin, vmax
	
	MatrixGPU X; // (Pi, Qi, li, vi, pi, qi, vai, Pci ..., Qci... , lci...) !!!!!
	MatrixGPU Ypre;
	MatrixGPU Y; // (Pi, Qi, li, vi, pi, qi, vai, Pci ..., Qci... , lci...) !!!!!
	//MatrixGPU YTrans; // (Pi,Qi,vi,li,pi,qi,pai,qai,lai,vij) !!!
	MatrixGPU Mu;

	MatrixGPU Chat;

	MatrixGPU Hinv;
	MatrixGPU Q;

	MatrixCPU nChildCPU;
	MatrixGPU Childs;
	MatrixGPU PosChild; 
	MatrixGPU Ancestor;
	MatrixGPU CoresLineBus;

	MatrixGPU sizeOPFADMMGPU;
	MatrixGPU sizeOPFADMMGPUBig; // size : sizeOPFTotal

	MatrixCPU resF;

	// Local resolution
	MatrixGPU tempN2; // size : (_nAgent*2, 1)
	MatrixGPU tempB2; // size : (_nBus  *2, 1)
	MatrixGPU CoresSoloBusAgent;
	MatrixGPU Pn;
	MatrixGPU Pmin;
	MatrixGPU Pmax;
	MatrixGPU PnTmin;
	MatrixGPU PnTmax;
	MatrixGPU PnMoy;
	MatrixGPU PnPre;
	MatrixGPU MuL;
	MatrixGPU PnTilde;
	MatrixGPU Bp1;
	MatrixGPU Bpt1;
	MatrixGPU Bpt2;
	MatrixGPU Apt1;
	MatrixGPU Apt2;
	

	// special GPU
	MatrixGPU _CoresAgentBus;
	MatrixGPU _CoresAgentBusBegin;
	MatrixGPU tempL;
	MatrixGPU _indiceBusBegin; // size : nBus
	MatrixGPU _indiceBusBeginBig; // size : sizeOPFTotal
	MatrixGPU _indiceChildBegin;
	MatrixGPU nChild;


};

//__global__ void setAncestorChild(float* coresLineBus, float* mat2, int N);




__global__ void defineSizeBig(float* sizeOPFADMMbig, float* nChild, float* CoresBusBegin, float* sizeOPFADMM, float* CoresBusBeginBig);

__global__ void divideMultiplyByNagentByBus(float* Apt1, float* Apt2, float* PnTilde, float* PnTmin, float* PnTmax, float* nAgentByBus, float rhol, int nBus);


__global__ void initDFSPQ(float* X, float* nChild, float* Childs, float* indiceBusBegin, float* indiceChildBegin, int nBus);

__global__ void initPQV(float* X, float* indiceBusBegin, float* nAgentByBus, float* PnTilde, int nBus);
__global__ void initPQ(float* X, float* indiceBusBegin, float* nAgentByBus, float* PnTilde, int nBus);

__global__ void communicateX(float* X, float* nChild, float* Ancestor, float* Childs, float* indiceBusBegin, float* indiceChildBegin, int nBus);


template <unsigned int blockSize>
__global__ void updateChatGPU(float* Chat, float* Y, float* MU, float* nChild, float* Ancestor, float* posChild, float* Childs, float* indiceBusBegin, float* indiceChildBegin,
	float _rho, int nBus);

__global__ void updateBpt2(float* Bpt2, float* Chat, float* nAgentByBus, int nBus);

template <unsigned int blockSize>
__global__ void updatePnPGPUSharedResidual(float* Pn, float* PnPre, float* PnMoy, float* PnTilde, float* MUL, float* nAgentByBus, float _rhol, float* Ap2, float* Cp, float* Pmin,
	float* Pmax, float* Apt1, float* Apt2, float* Bpt2, float* CoresSoloBusAgent, float* CoresBusAgent, float* CoresBusAgentBegin, float eps, int nIterLMax, int nAgent, int nBus);

__global__ void updateXOPFADMM(float* X, float* Chat, float* Vbound, float* PnTilde, float* nAgentByBus, float* nChild, float* indiceBusBegin, int nBus);

template <unsigned int blockSize>
__global__ void updatePnPGPUSharedResidual(float* Pn, float* PnPre, float* PnMoy, float* PnTilde, float* MUL, float* nAgentByBus, float _rhol, float* Ap2, float* Cp, float* Pmin,
	float* Pmax, float* Apt1, float* Apt2, float* Bpt2, float* CoresSoloBusAgent, float* CoresBusAgent, float* CoresBusAgentBegin, float eps, int nIterLMax, int nAgent, int nBus);

