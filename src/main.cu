
#include "../head/main.cuh"

// pour l'agent des pertes de Q, lui permettre de consommer et de vendre peut poser probl�me 
// car il peut faire les 2 alors que l'on aimerait qu'il soit inactif, en vrai il fait juste intermediaire mais bon...


// mingw32 - make.exe

// Simulation

int main(int argc, char* argv[]) {

	srand(time(nullptr));
	std::cout.precision(6);
	
	//std::cout << "test Utilities err =" << testUtilities() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	
	
	try
	{
	
		//testOPF();
		
		SimuCompareISGT();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	

	return 0;
}



void SimuCompareISGT() {
	std::string fileName = "ComparaisonOPFISGT_200_VAgent.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	/*const int nMethode = 5;
	std::vector<int> indices = { 0, 1, 2, 3, 4 };
	std::string methodesName[nMethode] = { "PDIPM", "OPFADMM", "OPFADMM2","OPFADMMGPU", "OPFADMMGPU2" };*/
	const int nMethode = 4;
	std::vector<int> indices = { 0, 1, 2, 3};
	std::string methodesName[nMethode] = { "OPFADMM", "OPFADMM2","OPFADMMGPU", "OPFADMMGPU2" };
	MethodOPF* methodes[nMethode];
	
	methodes[0] = new OPFADMM;
	methodes[1] = new OPFADMM2;
	methodes[2] = new OPFADMMGPU;
	methodes[3] = new OPFADMMGPU2;


	if (nMethode != indices.size()) {
		throw std::domain_error("not good number of methods");
	}

	// Market
	float Pconso = 0.05;
	float dPconso = 0.02;
	float Propcons = 0.5;
	float PropNFleGen = 0.25;
	float PropGen = 1 - PropNFleGen - Propcons;
	float bProd = 5;
	float dbProd = 2;
	float Pprod = 0.05;
	float dPprod = 0.02;
	float gamma = 4; // inutile car pas de trade
	float dgamma = 2; // idem
	float dQ = 0.05;
	float length = 0.001;
	float dlength = 0.0005;
	float factorAgent = 5;


	// cases
	int nNBus = 1;
	int nBusMax = 200;
	const int offsetBus = 0;
	int nSimu = 50;
	int million = 1000000;
	int nCasAgent = 1;
	int nCasBuses = 1;
	int nCas = nCasAgent * nCasBuses;


	// simulation
	int iterGlobal = 20000;
	int iterLocal = 1000;
	int stepG = 10;
	int stepL = 10;
	float epsG = 0.01f;
	float epsL = 0.0005f;
	float rho = 2;



	MatrixCPU Param(1, 22);
	Param.set(0, 0, nBusMax);
	Param.set(0, 1, nNBus);
	Param.set(0, 2, length);
	Param.set(0, 3, dlength);
	Param.set(0, 4, Pconso);
	Param.set(0, 5, dPconso);
	Param.set(0, 6, Pprod);
	Param.set(0, 7, dPprod);
	Param.set(0, 8, dQ);
	Param.set(0, 9, Propcons);
	Param.set(0, 10, PropGen);
	Param.set(0, 11, nSimu);
	Param.set(0, 12, nCasAgent);
	Param.set(0, 13, nCasBuses);
	Param.set(0, 14, nMethode);
	Param.set(0, 15, rho);
	Param.set(0, 16, epsG);
	Param.set(0, 17, epsL);
	Param.set(0, 18, iterGlobal);
	Param.set(0, 19, iterLocal);
	Param.set(0, 20, stepG);
	Param.set(0, 21, stepL);



	Param.saveCSV(fileName, mode);


	MatrixCPU Buses(1, nNBus);
	MatrixCPU temps(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU iters(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU PResult(nMethode * nSimu, nNBus, nanf(""));

	MatrixCPU QResult(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU ResR(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU ResS(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU ResV(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU CountRelax(nMethode * nSimu, nNBus, nanf(""));
	MatrixCPU Fc(nMethode * nSimu, nNBus, nanf(""));
	System sys;
	StudyCase cas;
	Simparam res;
	sys.setIter(iterGlobal, iterLocal);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(stepG, stepL);
	sys.setRho(rho);

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;

	for (int bus = 0; bus < nNBus; bus++) {
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;
		int buses = offsetBus > 0 ? bus * (nBusMax - offsetBus) / max(nNBus - 1, 1) + offsetBus : (bus + 1) * nBusMax / nNBus;
		Buses.set(0, bus, buses);

		int nDeep = 2 * buses;
		int nBranch = buses;
		int j = bus;

		int agents = factorAgent * buses;
		for (int simu = 0; simu < nSimu; simu++) {

			std::cout << "-";
			cas.genGridBT(buses, nBranch, nDeep, length, dlength);
			cas.genAgentsAC(agents, Propcons, PropGen, Pconso, dPconso, bProd, dbProd, dQ, Pprod, dPprod, gamma, dgamma);
			cas.genLinkGridAgent();

			sys.setStudyCase(cas);
			std::random_shuffle(indices.begin(), indices.end());
			for (int i = 0; i < nMethode; i++) {
				int k = indices[i] * nSimu + simu;
				//std::cout << " Simu  " << simu << " methode  " << i << std::endl;
				//if (indices[i] > 0) {
				sys.setMethod(methodes[indices[i]]);
				t1 = std::chrono::high_resolution_clock::now();
				res = sys.solve();
				t2 = std::chrono::high_resolution_clock::now();
				int count = methodes[indices[i]]->feasiblePoint();
				MatrixCPU Pn = res.getPn();

				MatrixCPU ResF = res.getRes();
				int iter = res.getIter();

				PResult.set(k, j, Pn.get(1, 0));
				QResult.set(k, j, Pn.get(agents + 2, 0));
				temps.set(k, j, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
				iters.set(k, j, iter);
				ResR.set(k, j, ResF.get(0, (iter - 1) / stepG));
				ResS.set(k, j, ResF.get(1, (iter - 1) / stepG));
				ResV.set(k, j, ResF.get(2, (iter - 1) / stepG));
				CountRelax.set(k, j, count);
				Fc.set(k, j, res.getFc());
				sys.resetParam();
				//}
			}
		}

		std::cout << std::endl;
		//std::cout << "-|-|-|-|-|-|-|-|-  -|-|-|-|-|-|-|-|-|" << std::endl;

	}


	Buses.saveCSV(fileName, mode);

	PResult.saveCSV(fileName, mode);
	QResult.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);
	ResR.saveCSV(fileName, mode);
	ResS.saveCSV(fileName, mode);
	ResV.saveCSV(fileName, mode);
	Fc.saveCSV(fileName, mode);
	CountRelax.saveCSV(fileName, mode);
	iters.display();


	for (int i = 0; i < nMethode; i++) {
		DELETEB(methodes[i]);
	}
	sys.setMethod(nullptr);
}

/* Test fonctionnel */

void testOPF()
{
	StudyCase cas;
	int million = 1000000;
	int choseCase = 1;// rxRX
	std::string fileName = "OPFISGTResidualscase69.csv"; //"TimeByBlockPF";
	std::string chosenCase = "";
	float Power = 0;
	int nMethode = 4;
	bool methodeToCompute[] = { true, true, true, true };
	bool saveResiduals = false;
	MatrixCPU results(5, nMethode, -1);
	int method = 0;
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	float rhoInit = 50;
	
	switch (choseCase)
	{
	case 0:
		cas.SetAC2node();

		cas.display();
		break;
	case 1:
		chosenCase = "case69"; //case10ba case4_dist case85 case69 ?, pas radial : case30 case300
		cas.SetACFromFile(chosenCase);
		cas.display();
		break;
	case 2:
		cas.SetEuropeTestFeeder();
		cas.display();
		break;
	case 4:
		cas.SetAC3Bus();
		cas.display();
		break;
	case 5:
		chosenCase = "RandRadial";
		cas.genGridBT(30, 80, 200, 0.001, 0.0005);
		cas.genAgentsAC(30, 0.5, 0.5, 1, 0.5, 1, 0.1, 0.5, 1, 0.5, 1, 0.2);
		cas.genLinkGridAgent();
		cas.display();
		break;
	default:
		throw std::invalid_argument("unknown choseCase");
		break;
	}
	OPFADMM opfADMM;
	OPFADMM2 opfADMM2;
	OPFADMMGPU opfADMMGPU;
	OPFADMMGPU2 opfADMMGPU2;
	
	Simparam param(cas.getNagent(), cas.getNLine(true), true);
	float epsL = 0.0001;
	param.setEpsL(epsL);
	param.setEpsG(0.001f);
	param.setItG(10000); //500000
	param.setItL(2000);
	param.setStep(1, 1);
	
	//for (int i = 0; i < 5; i++) {
	param.setRho(rhoInit);
	Simparam res(param);
	int nAgent = cas.getNagent();
	MatrixCPU Pn;
	bool radial = cas.isRadial();
	std::cout << "*********************************** OPFADMM ************************************" << std::endl;


	/*param.display(1);*/
	if (radial && methodeToCompute[method])
	{
		t1 = std::chrono::high_resolution_clock::now();
		opfADMM.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		if (saveResiduals) {
			MatrixCPU Residuals(res.getRes());
			Residuals.saveCSV(fileName);
		}
		

		Pn.display();
		//opfADMM.display();

		results.set(0, method, Pn.get(1, 0));
		results.set(1, method, Pn.get(nAgent + 1, 0));
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());
		res.display();
		opfADMM.feasiblePoint();
	}


	

	method++;
	std::cout << "********************************** OPFADMM 2 ***********************************" << std::endl;


	/*param.display(1);*/
	if (radial && methodeToCompute[method])
	{
		t1 = std::chrono::high_resolution_clock::now();
		opfADMM2.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();
		//opfADMM2.display();
		if (saveResiduals) {
			MatrixCPU Residuals(res.getRes());
			Residuals.saveCSV(fileName);
		}

		results.set(0, method, Pn.get(1, 0));
		results.set(1, method, Pn.get(nAgent + 1, 0));
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());
		res.display();
		opfADMM2.feasiblePoint();
	}

	
	method++;

	
	std::cout << "*********************************OPFADMM GPU**********************************" << std::endl;

	param.setEpsL(epsL / 20);
	if (radial && methodeToCompute[method])
	{
		t1 = std::chrono::high_resolution_clock::now();
		opfADMMGPU.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();
		//opfADMMGPU.display();
		if (saveResiduals) {
			MatrixCPU Residuals(res.getRes());
			Residuals.saveCSV(fileName);
		}

		results.set(0, method, Pn.get(1, 0));
		results.set(1, method, Pn.get(nAgent + 1, 0));
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());

		res.display();
		opfADMMGPU.feasiblePoint();
	}

	

	method++;
	std::cout << "*****************************OPFADMM 2 GPU****************************************" << std::endl;

	if (radial && methodeToCompute[method])
	{
		t1 = std::chrono::high_resolution_clock::now();
		opfADMMGPU2.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		Pn = res.getPn();
		Pn.display();
		if (saveResiduals) {
			MatrixCPU Residuals(res.getRes());
			Residuals.saveCSV(fileName);
		}
		//opfADMMGPU.display();

		results.set(0, method, Pn.get(1, 0));
		results.set(1, method, Pn.get(nAgent + 1, 0));
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());

		res.display();
		opfADMMGPU2.feasiblePoint();
	}

	

	method++;


	
		int iterG = 50;
		param.setStep(1, 1);
		param.setItG(iterG);
		param.setEpsL(epsL);

		res.setStep(1, 1);
		res.setItG(iterG);
		t1 = std::chrono::high_resolution_clock::now();
		opfPDIPM.solve(&res, param, cas);
		t2 = std::chrono::high_resolution_clock::now();
		//opfPDIPM.display();
		Pn = res.getPn();
		Pn.display();
		if (saveResiduals) {
			MatrixCPU Residuals(res.getRes());
			Residuals.saveCSV(fileName);
		}
		results.set(0, method, Pn.get(1, 0));
		results.set(1, method, Pn.get(nAgent + 1, 0));
		results.set(2, method, res.getFc());
		results.set(3, method, (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / million);
		results.set(4, method, res.getIter());
	}
	
	method++;
	std::cout << "*****************************************************************************" << std::endl;
	std::cout << "OPFADMM -" << " OPFADMM 2 "  << " OPFADMMGPU -" << " OPFADMM 2 -" << std::endl;
	results.display();


}



