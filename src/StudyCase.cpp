#include "../head/StudyCase.h"





float StudyCase::rand1() const
{
	float a = (float)(rand()) / ((float)(RAND_MAX));
	return a;
}

int StudyCase::randab(int a, int b) const
{
	return a + (rand() % (b - a));
}

void StudyCase::genCoresBusAgent(bool all)
{

	if (all) {
		_CoresBusAgent = MatrixCPU(_nBus, _nAgent);
		for (int n = 0; n < _nAgent; n++) {
			int bus = _CoresBusAgentLin.get(n, 0);
			_CoresBusAgent.set(bus, n, 1);
		}
	}

	_nAgentByBus = MatrixCPU(_nBus, 1);
	_CoresAgentBusLinBegin = MatrixCPU(_nBus, 1);
	_CoresAgentBusLin = MatrixCPU(_nAgent, 1);

	for (int i = 0; i < _nAgent; i++) {
		int bus = _CoresBusAgentLin.get(i, 0);
		_nAgentByBus.increment(bus, 0, 1);
	}


	int debut = 0;
	int* decompteAgent = new int[_nBus];
	for (int b = 0; b < _nBus; b++) {
		_CoresAgentBusLinBegin.set(b, 0, debut);
		debut += _nAgentByBus.get(b, 0);
		decompteAgent[b] = 0;
	}

	for (int n = 0; n < _nAgent; n++) {
		int bus = _CoresBusAgentLin.get(n, 0);
		int indice = _CoresAgentBusLinBegin.get(bus, 0) + decompteAgent[bus];
		decompteAgent[bus]++;
		_CoresAgentBusLin.set(indice, 0, n);
	}
	DELETEA(decompteAgent);
}

void StudyCase::initMat()
{
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G = A*I
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent); // I
	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) { // par defaut
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}

}


void StudyCase::setReduce(bool toReduce1)
{
	if (DC) {
		toReduce = toReduce1;
		SCDCG.toReduce = toReduce1;
	}
	else {
		std::cout << " Warning : Nothing happens on the grid side, the study case must be set before" << std::endl;
		toReduce = toReduce1;
	}

}



StudyCase::StudyCase()
{
	 _timeInit = 0;
	 initMat();
	 DC = true;
	 srand(time(nullptr));
}
StudyCase::StudyCase(int nAgent, float P, float dP, float a, float da, float b, float db, float propCons, float propPro)
{
	clock_t t = clock();
	
	SCAg = StudyCaseAgent(nAgent, P, dP, a, da, b, db, propCons, propPro);
	DC = true;
	int nLineConstraint = SCDCG.getNLineConstraint();
	//std::cout << "nLineConstraint = " << nLineConstraint << std::endl;
	initMat();
	genCoresBusAgent(true);
	
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

}
StudyCase::StudyCase(int nAgent, float P0, float dP, float b, float db, float propCons)
{
	clock_t t = clock();
	SCAg = StudyCaseAgent(nAgent, P0, dP, b, db, propCons);
	int nLineConstraint = SCDCG.getNLineConstraint();
	initMat();
	genCoresBusAgent(true);
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

StudyCase::StudyCase(int nAgent, float P, float dP, float Q, float dQ, float a, float da, float aQ, float daQ, float b, float db, float Gamma, float dGamma, float propCons, float propGenNFle, float propPro){

	clock_t t = clock();

	
	SCAg = StudyCaseAgent(nAgent, P, dP, Q, dQ, a, da, a, daQ, b, db,Gamma, dGamma, propCons, propGenNFle, propPro);
	
	DC = false;
	
	_nAgent = SCAg.getNagent();
	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}
	
	_nBus = SCACG.getNBus();
	

	genCoresBusAgent();
	

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

}


StudyCase::StudyCase(const StudyCase& s)
{
	clock_t t = clock();
	SCAg = s.SCAg;
	SCACG = s.SCACG;
	SCDCG = s.SCDCG;

	
	_name = s._name;
	_Sbase = s._Sbase;
	toReduce = s.toReduce;
	DC = s.DC;
	_nAgent = s._nAgent;
	_nBus = s._nBus;
	_nLine = s._nLine;

	
	_CoresBusAgent = s._CoresBusAgent; // I
	_SensiPower = s._SensiPower; // G
	_SensiPowerReduce = s._SensiPowerReduce;
	_Distance = s._Distance;

	_CoresBusAgentLin = s._CoresBusAgentLin;
	_CoresAgentBusLin = s._CoresAgentBusLin;
	_CoresAgentBusLinBegin = s._CoresAgentBusLinBegin;
	_nAgentByBus = s._nAgentByBus;
	

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}
StudyCase::StudyCase(std::string fileName)
{
	clock_t t = clock();
	SCAg = StudyCaseAgent(fileName);
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

}
StudyCase& StudyCase::operator= (const StudyCase& s)
{
	clock_t t = clock();
	SCAg = s.SCAg;
	SCACG = s.SCACG;
	SCDCG = s.SCDCG;


	_name = s._name;
	_Sbase = s._Sbase;
	toReduce = s.toReduce;

	DC = s.DC;
	_nAgent = s._nAgent;
	_nBus = s._nBus;
	_nLine = s._nLine;


	_CoresBusAgent = s._CoresBusAgent; // I
	_SensiPower = s._SensiPower; // G
	_SensiPowerReduce = s._SensiPowerReduce;
	_Distance = s._Distance;

	_CoresBusAgentLin = s._CoresBusAgentLin;
	_CoresAgentBusLin = s._CoresAgentBusLin;
	_CoresAgentBusLinBegin = s._CoresAgentBusLinBegin;
	_nAgentByBus = s._nAgentByBus;

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
	return *this;
}

void StudyCase::UpdateP0(MatrixCPU* P0)
{
	SCAg.UpdateP0(P0);
}
void StudyCase::genBetaUniforme(float beta)
{
	SCAg.genBetaUniforme(beta);
}

void StudyCase::genBetaDistance(float s)
{
	int nAgent = SCAg.getNagent();
	if (_Distance.getNLin() != nAgent) {
		setDistance();
	}

	SCAg.genBetaDistance(s, &_Distance);
}

void StudyCase::genBetaDistanceByZone(MatrixCPU* Mats)
{
	int nAgent = SCAg.getNagent();
	if (_Distance.getNLin() != nAgent) {
		setDistance();
	}
	// verification que c'est l�gal : 
	std::cout << "work in progress" << std::endl;
	throw std::invalid_argument("WIP");
	// differencier DC ou AC
	// 
	if (DC) {
		SCAg.genBetaZone(Mats, &_Distance, SCDCG.getZones());
	}
	else {
		SCAg.genBetaZone(Mats, &_Distance, SCACG.getZones());
	}
}

void StudyCase::setDistance(bool alreadyDefine, std::string name )
{
	int nAgent = SCAg.getNagent();
	int nLine = SCDCG.getNLine(true);
	_Distance = MatrixCPU(nAgent, nAgent);
	if (alreadyDefine) {
		_Distance.setFromFile(name);
	}
	else {
		std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
		for (int i = 0; i < nAgent; i++) {
			for (int j = i + 1; j < nAgent; j++) {
				float sum = 0;
				for (int k = 0; k < nLine; k++) {
					float p = _SensiPower.get(k, i) - _SensiPower.get(k, j);
					sum = sum + fabs(p);
				}
				_Distance.set(i, j, sum);
				_Distance.set(j, i, sum);
			}
		}
		_Distance.display();
		_Distance.saveCSV(name, mode, 0, " ");
	}
}

bool StudyCase::isAC() const
{
	return !DC;
}

bool StudyCase::isRadial() const
{
	if (DC) {
		return false;
	}
	return SCACG.radial;
}



void StudyCase::setLineLimitMin(float min)
{
	SCDCG.setLineLimitMin(min);
}

void StudyCase::setLineLimitRelaxation(float eps)
{
	SCDCG.setLineLimitRelaxation(eps);
}

///////////////////////////////////////////////////////////////////////////////
// Set StudyCase
///////////////////////////////////////////////////////////////////////////////

void StudyCase::Set29node()
{
	
	clock_t t = clock();
	SCAg.Set29node();
	DC = true;
		
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();
	
	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent/2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}/**/
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	
	genCoresBusAgent(true);
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);


	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);


	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}


void StudyCase::Set39Bus(std::string path, bool alreadyDefine)
{
	clock_t t = clock();
	_CoresBusAgentLin = SCAg.Set29node();
	SCDCG.Set39Bus(path, alreadyDefine);
	DC = true;

	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();

	std::string filename = path + "Network39.txt";
	
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G

	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string fileName = path + "SensiPower39node.txt";
	std::string fileName3 = path + "SensiPowerReduce39node.txt";

	genCoresBusAgent(true);

	if (alreadyDefine) {
		_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent);
		_SensiPower.setFromFile(fileName);
		_SensiPowerReduce.setFromFile(fileName3);
	}
	else {

		_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
		_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
		_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
		_SensiPower.saveCSV(fileName, mode, 0, " ");
		_SensiPowerReduce.saveCSV(fileName3, mode, 0, " ");

	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
	
}
void StudyCase::SetAC39Bus(std::string path, bool alreadyDefine)
{
	clock_t t = clock();

	_CoresBusAgentLin = SCAg.Set29node(true);
	DC = false;
	SCACG.SetAC39Bus(path, alreadyDefine);
	
	int nBus = SCACG.getNBus();
	int nAgent = SCAg.getNagent();
	
	genCoresBusAgent();


	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::SetAC3Bus(std::string path)
{
	//case3.m
	_Sbase = 100; // MW car matlab
	clock_t t = clock();
	
	_CoresBusAgentLin = SCAg.Set3Bus(true);
	DC = false;
	
	SCACG.SetAC3Bus();

	
	_nBus = SCACG.getNBus(); // 3
	_nAgent = SCAg.getNagent(); // 4

	genCoresBusAgent();


	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::SetAC2node() {
	_Sbase = 1; // MW 
	clock_t t = clock();
	
	_CoresBusAgentLin = SCAg.Set2node(true);
	DC = false;
	
	_nBus = SCACG.getNBus(); // 3
	_nAgent = SCAg.getNagent(); // 4


	genCoresBusAgent();

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::SetACFromFile(std::string name, std::string path)
{
	_CoresBusAgentLin = SCAg.SetACFromFile(name, path);
	DC = false;
	SCACG.SetACFromFile(name, path);
	_nBus = SCACG.getNBus();
	_nAgent = SCAg.getNagent();
	
	genCoresBusAgent();
	
}

void StudyCase::SetStudyCase(std::string path, std::string name, MatrixCPU* P0, bool alreadyDefine)
{
	clock_t t = clock();
	std::string fileName = path + "SensiPower" + name + ".txt";
	std::string fileName3 = path + "SensiPowerReduce" + name + ".txt";
	_name = name;
	//std::cout << "agent" << std::endl;
	SCAg.SetStudyCaseAgent(path, name, P0);
	int nBus = SCAg.getNCons();
	int nAgent = SCAg.getNagent();
	//std::cout << "grid" << std::endl;
	SCDCG.SetStudyCaseDCGrid(path, name, nBus, alreadyDefine);
	int nLine = SCDCG.getNLine(true);
	//std::cout << "link" << std::endl;
	_SensiPower = MatrixCPU(nLine, nAgent); // G
	_CoresBusAgent = MatrixCPU(nBus, nAgent); // I
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;


	if (alreadyDefine) {
		int _nLineConstraint = SCDCG.getNLineConstraint();
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, nAgent);
		_SensiPower.setFromFile(fileName);
		_SensiPowerReduce.setFromFile(fileName3);
	}
	else {
		MatrixCPU fileCoresBus = SCDCG.getfileCoresBus();
		MatrixCPU GenBus = SCAg.getGenBus();

		int idBusMax = fileCoresBus.max2();
		MatrixCPU fileBusAgent(idBusMax + 1, 1, -1); // si reste � -1, le bus n'existe pas

		//std::cout << "agent" << std::endl;
		for (int i = 0; i < nBus; i++) {
			int bus = fileCoresBus.get(i, 0);
			fileBusAgent.set(bus, 0, i);
		}

		//std::cout << _nLineConstraint << std::endl;
		//std::cout << "agent" << std::endl;
		for (int i = 0; i < nAgent; i++) {
			if (i < nBus) { // le bus correspond directement pour les conso
				int bus = i;
				_CoresBusAgent.set(bus, i, 1);
			}
			else {
				int idGen = i - nBus;
				int bus = fileBusAgent.get(GenBus.get(idGen, 0), 0);
				_CoresBusAgent.set(bus, i, 1);
			}
		}

		MatrixCPU SensiBusLine = SCDCG.getPowerSensiBus(true);
		MatrixCPU SensiBusLineReduce = SCDCG.getPowerSensiBusReduce();
		_SensiPower.multiply(&SensiBusLine, &_CoresBusAgent);

		_SensiPower.saveCSV(fileName, mode);
		_SensiPowerReduce.saveCSV(fileName3, mode);
	}
	//_SensiPower.display();
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}



void StudyCase::SetEuropeTestFeeder(std::string path, int typeOfAgentGen, int beggining)
{
	switch (typeOfAgentGen)
	{
	case 0: // 55 consumers, 1 productor as in the File
		std::cout << " agent " << std::endl;
		_CoresBusAgentLin = SCAg.SetEuropeTestFeeder(path, beggining);
		break;
	default:
		throw std::invalid_argument("WIP not implemented");
		break;
	}
	
	DC = false;
	
	SCACG.SetEuropeTestFeeder(path);
	_nBus = SCACG.getNBus();
	_nAgent = SCAg.getNagent();

	genCoresBusAgent();


}

void StudyCase::Set3Bus(std::string path) {
	clock_t t = clock();
	std::cout << "set agent" << std::endl;
	_CoresBusAgentLin = SCAg.Set3Bus();
	DC = true;
	std::cout << "set grid" << std::endl;
	SCDCG.Set3Bus();


	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();


	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	

	genCoresBusAgent(true);
	
	
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
	
	
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set4Agent()
{
	clock_t t = clock();
	
	SCAg.Set4Agent();
	DC = true;

	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();

	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G

	genCoresBusAgent(true);

	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);


	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set2node() 
{
	clock_t t = clock();
	
	SCAg.Set2node();
	DC = true;
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();

	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G

	genCoresBusAgent(true);

	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);

	_SensiPower.display();

	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);


	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set4nodeBis(std::string path)
{
	// cas d'�tude pour simuler le cas d'EVA pendant son stage
	clock_t t = clock();
	
	
	
	_CoresBusAgentLin = SCAg.Set4nodeBis();
	DC = true;

	SCDCG.Set4nodeBis(path);
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();
	
	
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;



	genCoresBusAgent(true);
	
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);

	
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set2nodeConstraint(float lim)
{
	clock_t t = clock();

	SCAg.Set2node();
	DC = true;
	SCDCG.Set2nodeConstraint(lim);
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();
	
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}
	genCoresBusAgent(true);
	

	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);

}

void StudyCase::SetEuropeP0(const std::string& path, MatrixCPU* P0, bool alreadyDefine)
{
	clock_t t = clock();
	
	_name = "Europe";
	
	SCAg.SetEuropeP0(path, P0);
	DC = true;
	SCDCG.SetEuropeP0(path, alreadyDefine);
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();
	int nCons = SCAg.getNCons();
	//genBetaUniforme(0);

	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent);

	_CoresBusAgentLin = MatrixCPU(_nAgent, 1); // Ilin
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string fileName = path + "SensiPowerEurope.txt";
	std::string fileName3 = path + "SensiPowerReduceEurope.txt";
	
	if (alreadyDefine) {
		_SensiPower.setFromFile(fileName);
		_SensiPowerReduce.setFromFile(fileName3);
	}
	else {
		MatrixCPU fileCoresBus = SCDCG.getfileCoresBus();
		MatrixCPU GenBus = SCAg.getGenBus();

		int idBusMax = fileCoresBus.max2();
		MatrixCPU fileBusAgent(idBusMax + 1, 1, -1); // si reste � -1, le bus n'existe pas
		

		for (int i = 0; i < _nBus; i++) {
			int bus = fileCoresBus.get(i, 0);
			fileBusAgent.set(bus, 0, i);
		}
	
		//std::cout << _nLineConstraint << std::endl;
		for (int i = 0; i < _nAgent; i++) {
			if (i < nCons) { // le bus correspond directement pour les conso
				int bus = i;
				_CoresBusAgentLin.set(i, 0, bus);
			}
			else {
				int idGen = i - nCons;
				int bus = fileBusAgent.get(GenBus.get(idGen, 0), 0);
				_CoresBusAgentLin.set(i, 0, bus);
			}
		}

		genCoresBusAgent(true);
		
		_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
		_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
		
		_SensiPower.saveCSV(fileName, mode);
		_SensiPowerReduce.saveCSV(fileName3, mode);
	}
	//_SensiPower.display();
	t = clock() - t;
	//_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::SetEuropeP0WithoutConstraint(const std::string& path, MatrixCPU* P0)
{
	clock_t t = clock();

	_name = "Europe";

	SCAg.SetEuropeP0(path, P0);
	DC = true;
	_nBus = SCDCG.getNBus();
	_nAgent = SCAg.getNagent();
	_nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();

	_CoresBusAgentLin = MatrixCPU(_nAgent, 1);
	for (int id = 0; id < _nAgent / 2; id++) {
		_CoresBusAgentLin.set(id, 0, 0);
	}
	for (int id = _nAgent / 2; id < _nAgent; id++) {
		_CoresBusAgentLin.set(id, 0, 1);
	}
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G

	genCoresBusAgent(true);

	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);


	_SensiPowerReduce = MatrixCPU(nLineConstraint, _nAgent); // Gred
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);


	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}


///////////////////////////////////////////////////////////////////////////////
// Generator
///////////////////////////////////////////////////////////////////////////////


void StudyCase::genGrid(int _nBus, int _nMajorLine, int _minorLine, float ReacMajor, float DeltaReacMajor, float ReacMinor, float DeltaReacMinor, float LlimitMajor, float dLlimitMajor, float LlimitMinor, float dLlimitMinor)
{
	std::cout << "work in progress" << std::endl;
	throw std::invalid_argument("WIP");
}

void StudyCase::genGridBT(int nBus, int Nbranch, int Ndeep, float length, float dlength)
{
	SCACG.genGridBT(nBus, Nbranch, Ndeep, length, dlength);
	DC = false;

}

void StudyCase::genGridHTB(int nBus, int nLine, int dnLine, float length, float dlength)
{
	SCACG.genGridHTB(nBus, nLine, dnLine, length, dlength);
	DC = false;
}


void StudyCase::genGridFromFile(std::string path, bool alreadyDefine)
{
	DC = true;
	SCDCG.genGridFromFile(path, alreadyDefine);
}

void StudyCase::genAgents(int nAgent, float propCons, float Pconso, float dPconso, float bProd, float dbProd, float Pprod, float dPprod, float Gamma, float dGamma)
{
	clock_t t = clock();
	SCAg.genAgents(nAgent, propCons, Pconso, dPconso, bProd, dbProd, Pprod, dPprod, Gamma, dGamma);
	DC = true;
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::genAgentsAC(int nAgent, float propCons, float propGenNFle, float Pconso, float dPconso, float bProd, float dQconso, float dbProd, float Pprod, float dPprod, float Gamma, float dGamma)
{
	
	SCAg.genAgentsAC(nAgent, propCons, propGenNFle, Pconso, dPconso, bProd, dQconso, dbProd, Pprod, dPprod, Gamma, dGamma);
	
}

void StudyCase::genLinkGridAgent()
{
	//std::cout << "gen link between agent" << std::endl;
	int nAgent = SCAg.getNagent();
	int nBus, nLine, nLineConstraint;
	if (DC) {
		nBus = SCDCG.getNBus();
		nLine = SCDCG.getNLine(true);
		nLineConstraint = SCDCG.getNLineConstraint();
		_CoresBusAgent = MatrixCPU(nBus, nAgent);
	}
	else {
		nBus = SCACG.getNBus();
		nLine = SCACG.getNLine();
		nLineConstraint = nLine;
		_nAgentByBus = MatrixCPU(nBus, 1);
		_nAgentByBus.set(0, 0, 1); // loss agent
		_CoresAgentBusLinBegin = MatrixCPU(nBus, 1);
		_CoresBusAgentLin = MatrixCPU(nAgent, 1);
		_CoresAgentBusLin = MatrixCPU(nAgent, 1);
	}
	
	for (int n = 1; n < nAgent; n++) {
		int bus = rand() % nBus;
		if (DC) {
			_CoresBusAgent.set(bus, n, 1);
		}
		else {
			_CoresBusAgentLin.set(n, 0, bus);
			_nAgentByBus.increment(bus, 0, 1);
		}
	}


	if (DC) {
		_SensiPower = MatrixCPU(nLine, nAgent); // G
		_SensiPowerReduce = MatrixCPU(nLineConstraint, nAgent); // Gred
		_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
		_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
	}
	else {
		int debut = 0;
		int* decompteAgent = new int[nBus];
		for (int b = 0; b < nBus; b++) {
			_CoresAgentBusLinBegin.set(b, 0, debut);
			debut += _nAgentByBus.get(b, 0);
			decompteAgent[b] = 0;
		}
		for (int n = 1; n < nAgent; n++) {
			int bus = _CoresBusAgentLin.get(n, 0);
			int indice = _CoresAgentBusLinBegin.get(bus, 0) + decompteAgent[bus];
			decompteAgent[bus]++;
			_CoresAgentBusLin.set(indice, 0, n);
		}
		DELETEA(decompteAgent);

	}
	//std::cout << "Fin gen link between agent" << std::endl;
}

void StudyCase::computeSensiPower()
{
	int nAgent = SCAg.getNagent();
	int nBus = SCDCG.getNBus();
	int nLine = SCDCG.getNLine();
	int nLineConstraint = SCDCG.getNLineConstraint();
	_SensiPower = MatrixCPU(nLine, nAgent); // G
	_SensiPowerReduce = MatrixCPU(nLineConstraint, nAgent); // Gred
	_SensiPower.multiply(&SCDCG.getPowerSensiBus(true), &_CoresBusAgent);
	_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
}

void StudyCase::genLineLimit(int nLine, float limit, float dlLimit)
{
	DC = true;
	SCDCG.genLineLimit(nLine, limit, dlLimit);
}

///////////////////////////////////////////////////////////////////////////////
// Getter
///////////////////////////////////////////////////////////////////////////////
float StudyCase::getSbase() const
{
	return _Sbase;
}


MatrixCPU StudyCase::getBeta() const
{
	return SCAg.getBeta();
}

MatrixCPU StudyCase::getC() const
{
	return SCAg.getC();
}

MatrixCPU StudyCase::geta() const
{
	return SCAg.geta();
}

MatrixCPU StudyCase::getb() const
{
	return SCAg.getb();
}

MatrixCPU StudyCase::getUb() const
{
	return SCAg.getUb();
}

MatrixCPU StudyCase::getLb() const
{
	return SCAg.getLb();
}

MatrixCPU StudyCase::getPmin() const
{
	return SCAg.getPmin();
}

MatrixCPU StudyCase::getPmax() const
{
	return SCAg.getPmax();
}

MatrixCPU StudyCase::getNvoi() const
{
	return SCAg.getNvoi();
}


MatrixCPU StudyCase::getPowerSensi() const
{
	if (toReduce) {
		return _SensiPowerReduce;
	}
	else {
		return _SensiPower;
	}
	
}

MatrixCPU StudyCase::getLineLimit() const
{
	return SCDCG.getLineLimit();
	
}

MatrixCPU StudyCase::getCoresLineBus() const
{
	if (DC) {
		return SCDCG.getCoresLineBus();
	}
	else {
		return SCACG.getCoresLineBus();
	}
	
}

MatrixCPU StudyCase::getCoresBusAgent() const
{
	return _CoresBusAgent;
}

MatrixCPU StudyCase::getCoresBusAgentLin() const
{
	return _CoresBusAgentLin;
}

MatrixCPU StudyCase::getCoresAgentBusLin() const
{
	return _CoresAgentBusLin;
}

MatrixCPU StudyCase::getCoresAgentBusLinBegin() const
{
	return _CoresAgentBusLinBegin;
}

MatrixCPU StudyCase::getNagentByBus() const
{
	return _nAgentByBus;
}

MatrixCPU StudyCase::getLineSuceptance() const
{
	return SCACG.getLineSuceptance();
}

MatrixCPU StudyCase::getLineReactance() const
{
	return SCACG.getLineReactance();
}

MatrixCPUD StudyCase::getLineSuceptanceD() const
{
	return SCACG.getLineSuceptanceD();
}

MatrixCPUD StudyCase::getLineReactanceD() const
{
	return SCACG.getLineReactanceD();
}

MatrixCPU StudyCase::getUpperBound() const
{
	return SCACG.getUpperBound();
}

MatrixCPU StudyCase::getLowerBound() const
{
	return SCACG.getLowerBound();
}

MatrixCPU StudyCase::getPobj()
{
	return SCAg.getPobj();
}

MatrixCPUD StudyCase::getPobjD()
{

	return SCAg.getPobjD();
}


MatrixCPUD StudyCase::getSolPF() const
{
	return SCACG.getSolPF();
}

double StudyCase::getV0() const
{
	return SCACG.getV0();
}

double StudyCase::gettheta0() const
{
	return SCACG.gettheta0();
}

MatrixCPU StudyCase::getZsRe() const
{
	return SCACG.getZsRe();
}

MatrixCPU StudyCase::getZsImag() const
{
	return SCACG.getZsImag();
}

MatrixCPU StudyCase::getYd() const
{
	return SCACG.getYd();
}

MatrixCPU StudyCase::getGlin() const
{
	return SCACG.getGlin();
}

MatrixCPU StudyCase::getBlin() const
{
	return SCACG.getBlin();
}

MatrixCPU StudyCase::getGlin2() const
{
	return SCACG.getGlin();
}

MatrixCPU StudyCase::getBlin2() const
{
	return SCACG.getBlin();
}

MatrixCPU StudyCase::getVoltageInit() const
{
	return SCACG.getVoltageInit();
}

MatrixCPUD StudyCase::getVoltageInitD() const
{
	return SCACG.getVoltageInitD();
}

MatrixCPUD StudyCase::getGlinD() const
{
	return SCACG.getGlinD();
}

MatrixCPUD StudyCase::getBlinD() const
{
	return SCACG.getBlinD();
}

MatrixCPU StudyCase::getNLines() const
{
	return SCACG.getNLines();
}

MatrixCPU StudyCase::getNLinesBegin() const
{
	return SCACG.getNLinesBegin();
}

MatrixCPU StudyCase::getCoresBusLin() const
{
	return SCACG.getCoresBusLin();
}

MatrixCPU StudyCase::getCoresVoiLin() const
{
	return SCACG.getCoresVoiLin();
}

float StudyCase::getTimeInit() const
{
	return _timeInit;
}

int StudyCase::getNagent() const
{
	return SCAg.getNagent();
}

int StudyCase::getNCons() const
{
	return SCAg.getNCons();
}

int StudyCase::getNLine(bool force) const
{
	if (DC) {
		return SCDCG.getNLine(force);
	}
	else {
		return SCACG.getNLine();
	}
	
}

int StudyCase::getNBus() const
{
	if (DC) {
		return SCDCG.getNBus();
	}
	else {
		return SCACG.getNBus();
	}
}

MatrixCPU StudyCase::getVoisin(int agent) const
{
	return SCAg.getVoisin(agent);
}
Agent StudyCase::getAgent(int agent) const
{
	return SCAg.getAgent(agent);
}

std::string StudyCase::getName() const
{
	return _name;
}

void StudyCase::removeLink(int i, int j)
{
	SCAg.removeLink(i, j);
}


void StudyCase::addLink(int i, int j)
{
	SCAg.addLink(i, j);
}

void StudyCase::setLineLimit(int line, float limit)
{
	if (DC) {
		SCDCG.setLineLimit(line, limit);
		_SensiPowerReduce = MatrixCPU(SCDCG.getNLineConstraint(), SCAg.getNagent()); // Gred
		_SensiPowerReduce.multiply(&SCDCG.getPowerSensiBusReduce(), &_CoresBusAgent);
	}
	else {
		SCACG.setLineLimit(line, limit);
	}
}

Agent StudyCase::removeAgent(int agent)
{
	
	return SCAg.removeAgent(agent);

}

void StudyCase::restoreAgent(Agent& agent, bool all) {

	SCAg.restoreAgent(agent, all);
}

void StudyCase::saveCSV(const std::string& fileName, bool all)
{
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;

	SCAg.saveCSV(fileName, all);
	if (DC) {
		SCDCG.saveCSV(fileName);
	}
	else {
		SCACG.saveCSV(fileName, all);
	}
	
	_SensiPowerReduce.saveCSV(fileName, mode);

}

void StudyCase::nextStepPobj()
{
	SCAg.nextStepPobj();
}


void StudyCase::display(int type) 
{
	if (type == 0) {
		SCAg.display();
		std::cout << "Agent to bus corespondance: " << std::endl;
		_CoresBusAgentLin.display();
		std::cout << "Bus to agent corespondance: " << std::endl;
		_CoresAgentBusLin.display();
	}
	else if (type == 1) {
		if (DC) {
			SCDCG.display();
		}
		else {
			SCACG.display();
		}	
	}
	else if (type == 2) {
		std::cout << "Grid Sensibility : " << std::endl;
		if (toReduce) {
			_SensiPowerReduce.display();
		}
		else {
			_SensiPower.display();
		}
	}
	
}

void StudyCase::displayLineCores(MatrixCPU* g, bool all)
{
	
	if (DC) {
		SCDCG.displayLineCores(g, all);
	}
	else {
		SCDCG.displayLineCores(g, all);
	}
	
}

StudyCase::~StudyCase()
{
#ifdef DEBUG_DESTRUCTOR
	std::cout << "case destructor" << std::endl;
#endif


}


